// Charjs JavaScripts
$("#loading").hide();
$(document).ajaxStart(function() {
    $('#loading').show();
});
$(document).ajaxComplete(function() {
    // console.log('test 123 close');
    $("#loading").hide();
});

(function($) {

    var base_url_part = window.location.pathname.split('/')[3];
    if (base_url_part == 'dashboard') {
        'use strict';

        var primary = '#7774e7',
            success = '#37c936',
            info = '#0f9aee',
            warning = '#ffcc00',
            danger = '#ff3c7e',
            primaryInverse = 'rgba(119, 116, 231, 0.1)',
            successInverse = 'rgba(55, 201, 54, 0.1)',
            infoInverse = 'rgba(15, 154, 238, 0.1)',
            warningInverse = 'rgba(255, 204, 0, 0.1)',
            dangerInverse = 'rgba(255, 60, 126, 0.1)',
            gray = '#f6f7fb',
            white = '#fff',
            dark = '#515365'

    
        $("#datepicker").show();
        $(".datepicker_end_class").hide();

        if ($('#tipe_view option:selected').val() == 'bulanan') {

            $("#datepicker").datepicker({
                format: "mm-yyyy",
                startView: "months",
                minViewMode: "months"
            });
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }
            today = mm + '-' + yyyy;
            $('#datepicker').val(today);
        }
        $('#tipe_view').click(function() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }

            if ($('#tipe_view option:selected').val() == 'bulanan') {

                $("#datepicker").datepicker({
                    format: "mm-yyyy",
                    startView: "months",
                    minViewMode: "months"
                });
                today = mm + '-' + yyyy;
                $('#datepicker').val(today);
                $("#datepicker").show();
                $("#datepicker_start").attr('type', 'hidden');
                $(".datepicker_end_class").hide();
                $("#datepicker_end").attr('type', 'hidden');
            } else {
                $('#datepicker').val(null);
                $("#datepicker").hide();
                $("#datepicker_start").attr('type', 'text');
                $(".datepicker_end_class").show();
                $("#datepicker_end").attr('type', 'text');
                today = dd + '-' + mm + '-' + yyyy;
                $('#datepicker_start').val(today);
                // console.log('period ' + $('#datepicker').val() + 'test ' + $("#datepicker_start").val());
            }
        });





  







        //Pie Chart
        var pieChart = document.getElementById("pie-chart");
        var pieCtx = pieChart.getContext('2d');
        var pieData = {
            labels: ["Pria", "Wanita"],
            datasets: [{
                fill: true,
                backgroundColor: [success, info, primary],
                data: [0, 0]
            }]
        };
        var pieConfig = new Chart(pieCtx, {
            type: 'pie',
            data: pieData,
            options: {
                maintainAspectRatio: false,
                tooltips: { enabled: false },
                hover: { mode: null },
                legend: {
                    display: true,
                    position: 'bottom',
                    labels: {
                        fontSize: 12,
                        boxWidth: 12,
                        usePointStyle: true,
                    }
                },

            }
        });
        //Pie Chart
        var pieChart2 = document.getElementById("pie-chart2");
        var pieCtx2 = pieChart2.getContext('2d');
        var pieData2 = {
            labels: ["Baru", "Lama"],
            datasets: [{
                fill: true,
                backgroundColor: [success, info, primary],
                data: [0, 0]
            }]
        };

        var pieConfig2 = new Chart(pieCtx2, {
            type: 'pie',
            data: pieData2,
            options: {
                maintainAspectRatio: false,
                tooltips: { enabled: false },
                hover: { mode: null },
                legend: {
                    display: true,
                    position: 'bottom',
                    labels: {
                        fontSize: 12,
                        boxWidth: 12,
                        usePointStyle: true,
                    }
                },

            }
        });

        // $(document).ready(function() {
        function tanggalPengunjung() {
            var tanggal_pengunjung = [];
            var today = new Date();
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();
            var month = mm;
            var startDate = new Date(yyyy, month - 1, 1);
            var endDate = new Date(yyyy, month, 0);
            // alert(' start day ' + startDate + ' end day ' + endDate);
            var daysOfYear = [];
            for (var d = startDate; d <= endDate; d.setDate(d.getDate() + 1)) {
                var day = new Date(d);
                var n = day.getDate();
                daysOfYear.push(n);
            }
            tanggal_pengunjung = daysOfYear;
            return tanggal_pengunjung;
        };



        var lineChart = document.getElementById("line-chart");
        var lineCtx = lineChart.getContext('2d');
        lineChart.height = 80;
        var label = tanggalPengunjung();
        var value = [];
        var lineConfig = new Chart(lineCtx, {
            type: 'line',
            data: {
                labels: label,
                datasets: [{
                    label: 'Pasien',
                    backgroundColor: infoInverse,
                    borderColor: info,
                    pointBackgroundColor: info,
                    borderWidth: 2,
                    data: value
                }]
            },

            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
   

      





        function getCaraBayar(datepicker, datepicker_start, datepicker_end, nama_poli) {
            $.ajax({
                // async: false,
                type: 'POST',
                url: $('#getCaraBayar').val(),
                data: {
                    tanggal: datepicker,
                    tanggal_start: datepicker_start,
                    tanggal_end: datepicker_end,
                    nama_poli: nama_poli
                },
                responseTime: 1000,
                success: function(d) {
                    if (d) {
                        // console.log(d);
                        $.each(d, function(index, val) {

                            var no = index + 1;
                            var tr_str = "<tr>" +
                                "<td align=''>" + no + " </td>" +
                                "<td align=''>" + val.png_jawab + "</td>" +
                                "<td align=''>" + val.jumlah + "</td>" +
                                "<td align=''>" + parseFloat(val.jumlah_persen).toFixed(2) + "</td>" +
                                "</tr>";
                            $('#tbody_CaraBayar').append(tr_str)

                        });


                    } else {
                        alret('TimeOut', "Please refresh Your Browser!");
                    }
                }


            });

        }

    

        function tanggalFormater(tanggal = null, tipe_view, tanggal_start = null, tanggal_end = null) {

            if (tipe_view == 'bulanan') {
                var dateval = tanggal;
                var bulan = parseInt(dateval.substr(0, 2) - 1);
                var tahun = parseInt(dateval.substr(-4));
                var date = ' Bulan ' + formatBulan(bulan) + ' ' + tahun
            } else {
                var dateval_start = tanggal_start;
                var hari_tanggal_start = parseInt(dateval_start.substr(0, 2));
                var bulan_start = parseInt(dateval_start.substr(3, 2) - 1);
                var tahun_start = parseInt(dateval_start.substr(-4));

                var dateval_end = tanggal_end;
                var hari_tanggal_end = parseInt(dateval_end.substr(0, 2));
                var bulan_end = parseInt(dateval_end.substr(3, 2) - 1);
                var tahun_end = parseInt(dateval_end.substr(-4));
                var date = ' Periode ' + hari_tanggal_start + ' ' + formatBulan(bulan_start) + ' ' + tahun_start +
                    ' sampai dengan ' + hari_tanggal_end + ' ' + formatBulan(bulan_end) + ' ' + tahun_end
            }
            return date;

        }

        function formatBulan(bulan) {
            switch (bulan) {
                case 0:
                    bulan = "Januari";
                    break;
                case 1:
                    bulan = "Februari";
                    break;
                case 2:
                    bulan = "Maret";
                    break;
                case 3:
                    bulan = "April";
                    break;
                case 4:
                    bulan = "Mei";
                    break;
                case 5:
                    bulan = "Juni";
                    break;
                case 6:
                    bulan = "Juli";
                    break;
                case 7:
                    bulan = "Agustus";
                    break;
                case 8:
                    bulan = "September";
                    break;
                case 9:
                    bulan = "Oktober";
                    break;
                case 10:
                    bulan = "November";
                    break;
                case 11:
                    bulan = "Desember";
                    break;
            }
            return bulan;
        }



    }

})(jQuery);