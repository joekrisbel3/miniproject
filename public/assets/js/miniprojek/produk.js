"use strict";
var produk = function() {
    var initProduk = function() {
       
        getKategoriProduk();

        function getKategoriProduk() {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: $("#getKaregoriProduk").val(),
                success: function(data) {
                    var options, index, select, option;
                    select = document.getElementById("kategori_id");
                   
                    options = data;
                    // select.options.length = 0;
                    select.add(new Option("-- Pilih Kategori Produk --", ""));

                    if (options.length > 0) {
                        for (index = 0; index < options.length; ++index) {
                            option = options[index];
                            select.options.add(new Option(option.nama_kategori, option.kategori_id));
                            
                        }
                    }
                }
            });
        };

        function getKategoriProdukEdit(id_kategori) {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: $("#getKaregoriProduk").val(),
                success: function(data) {
                    var options, index, select, option;
                    select = document.getElementById("kategori_id_edit");
                   
                    options = data;
                    // select.options.length = 0;
                    select.add(new Option("-- Pilih Kategori Produk --", ""));

                    if (options.length > 0) {
                        for (index = 0; index < options.length; ++index) {
                            option = options[index];
                            if(id_kategori == option.kategori_id ){
                                select.options.add(new Option(option.nama_kategori, option.kategori_id, true, true));
                            }else{
                                select.options.add(new Option(option.nama_kategori, option.kategori_id));
                            }
                            
                        }
                    }
                }
            });
        };

        // $("#editId").click(function(){
        $("button[name='editId']").on("click", function() {
            var id = $(this).data("id");
            var namaproduct = $(this).data("namaproduct");
            var kategoriid = $(this).data("kategoriid");
            var namaproductseo = $(this).data("namaproductseo");
            var harga = $(this).data("harga");
            var gambar = $(this).data("gambar"); 
            console.log(namaproduct);
            $("#idProduk").val(id);
            getKategoriProdukEdit(kategoriid);
            $("input[name='nama_product']").val(namaproduct);
            $("input[name='kategori_id']").val(kategoriid);
            $("input[name='nama_product_seo']").val(namaproductseo);
            $("input[name='harga']").val(harga);
            $("input[name='gambar']").val(gambar);
        });

      
    };

    return {

        init: function() {
            initProduk();
        },

    };

}();

jQuery(document).ready(function() {
    produk.init();
});