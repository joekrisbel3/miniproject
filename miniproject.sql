-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mini-project.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `id_users` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `jenis_kelamin` varchar(50) DEFAULT NULL,
  `password` varchar(250) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `nomor_hp` varchar(50) NOT NULL,
  `salt` varchar(250) NOT NULL,
  `images` text NOT NULL,
  `customer_type` varchar(50) NOT NULL,
  `is_aktif` enum('y','n') NOT NULL,
  PRIMARY KEY (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table mini-project.customer: ~2 rows (approximately)
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT IGNORE INTO `customer` (`id_users`, `email`, `tanggal_lahir`, `username`, `jenis_kelamin`, `password`, `full_name`, `nomor_hp`, `salt`, `images`, `customer_type`, `is_aktif`) VALUES
	(7, 'joekrisbel@gmail.com', '0000-00-00', 'joekrisbel', 'laki-laki', 'a3075222ec90aa63dbb10d8a053e62fe6128faaa27cac2.42572717', 'Yosef  Kribela Pandu Perwira', '', '6128faaa27cac2.42572717', '', 'Silver', 'y'),
	(8, 'joekrisbel3@gmail.com', '2020-12-30', 'joekrisbel3', 'laki-laki', '6d768d6f7fe79f58b1a0c95f3eff0afd612adf52029a49.48503839', 'Yosef  Kribela Pandu Perwira', '12345567', '612adf52029a49.48503839', 'poto_profil/1630167417_06f89afe34a829cb434b.jpg', 'Silver', 'y');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;

-- Dumping structure for table mini-project.customer_detail
CREATE TABLE IF NOT EXISTS `customer_detail` (
  `customer_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_alamat` varchar(250) NOT NULL,
  PRIMARY KEY (`customer_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table mini-project.customer_detail: ~5 rows (approximately)
/*!40000 ALTER TABLE `customer_detail` DISABLE KEYS */;
INSERT IGNORE INTO `customer_detail` (`customer_detail_id`, `customer_alamat`) VALUES
	(1, 'admin@gmail.com'),
	(3, 'user@gmail.com'),
	(4, 'jose@gmail.com'),
	(6, 'joekrisbel3@gmail.com'),
	(7, 'joekrisbel@gmail.com');
/*!40000 ALTER TABLE `customer_detail` ENABLE KEYS */;

-- Dumping structure for table mini-project.tabel_diskon
CREATE TABLE IF NOT EXISTS `tabel_diskon` (
  `diskon_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_diskon` varchar(250) DEFAULT NULL,
  `id_produk` int(11) DEFAULT NULL,
  `diskon_pesen` decimal(10,0) DEFAULT NULL,
  `diskon_value` decimal(10,0) DEFAULT NULL,
  `tanggal_berlaku` date DEFAULT NULL,
  `tanggal_berakhir` date DEFAULT NULL,
  PRIMARY KEY (`diskon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table mini-project.tabel_diskon: ~2 rows (approximately)
/*!40000 ALTER TABLE `tabel_diskon` DISABLE KEYS */;
INSERT IGNORE INTO `tabel_diskon` (`diskon_id`, `nama_diskon`, `id_produk`, `diskon_pesen`, `diskon_value`, `tanggal_berlaku`, `tanggal_berakhir`) VALUES
	(1, 'All Items Rp 2.000.000 discounts for Outright', 0, 0, 0, '2021-08-29', '2021-08-29'),
	(2, 'Water Purifier Rp 1.000.000 discounts for Installment', 0, 0, 0, '2021-08-29', '2021-08-29');
/*!40000 ALTER TABLE `tabel_diskon` ENABLE KEYS */;

-- Dumping structure for table mini-project.tabel_kategori
CREATE TABLE IF NOT EXISTS `tabel_kategori` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(50) DEFAULT NULL,
  `link` varchar(40) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `nama_kategori_seo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table mini-project.tabel_kategori: ~2 rows (approximately)
/*!40000 ALTER TABLE `tabel_kategori` DISABLE KEYS */;
INSERT IGNORE INTO `tabel_kategori` (`kategori_id`, `nama_kategori`, `link`, `parent`, `nama_kategori_seo`) VALUES
	(1, 'Purifier', 'purifier', NULL, NULL),
	(2, 'Purifier2', 'purifier2', NULL, NULL);
/*!40000 ALTER TABLE `tabel_kategori` ENABLE KEYS */;

-- Dumping structure for table mini-project.tabel_metode_pembayaran
CREATE TABLE IF NOT EXISTS `tabel_metode_pembayaran` (
  `metode_id` int(11) NOT NULL AUTO_INCREMENT,
  `metode_nama` varchar(250) DEFAULT NULL,
  `metode_jumlah_bulan` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`metode_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table mini-project.tabel_metode_pembayaran: ~3 rows (approximately)
/*!40000 ALTER TABLE `tabel_metode_pembayaran` DISABLE KEYS */;
INSERT IGNORE INTO `tabel_metode_pembayaran` (`metode_id`, `metode_nama`, `metode_jumlah_bulan`) VALUES
	(1, 'Outright (Full Price Payment)', '0'),
	(2, 'Installment 12 Months', '12'),
	(3, 'Installment 24 Months', '24'),
	(4, 'Installment 36 Months', '36');
/*!40000 ALTER TABLE `tabel_metode_pembayaran` ENABLE KEYS */;

-- Dumping structure for table mini-project.tabel_product
CREATE TABLE IF NOT EXISTS `tabel_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_product` varchar(100) NOT NULL,
  `discription_product` text NOT NULL,
  `nama_product_seo` varchar(140) NOT NULL,
  `harga` int(11) NOT NULL,
  `gambar` text NOT NULL,
  `kategori_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table mini-project.tabel_product: ~2 rows (approximately)
/*!40000 ALTER TABLE `tabel_product` DISABLE KEYS */;
INSERT IGNORE INTO `tabel_product` (`product_id`, `nama_product`, `discription_product`, `nama_product_seo`, `harga`, `gambar`, `kategori_id`) VALUES
	(1, 'Ombak Water Purifier', 'All right! Up! Move! Come on! Quickly! Quickly, Chewie. Han! Hurry! The fleet will be here any moment. Charges! Come on, come on! Oh, my! They\'ll be captured! Wa-wait! Wait, come back! Artoo, stay with me. Freeze! You Rebel scum.', 'ombak-water-purifier', 100000000, 'poto_produk/1630230970_995eedbe77eaeacb22c0.jpg', 1),
	(3, 'Villaem Water Purifier', 'All right! Up! Move! Come on! Quickly! Quickly, Chewie. Han! Hurry! The fleet will be here any moment. Charges! Come on, come on! Oh, my! They\'ll be captured! Wa-wait! Wait, come back! Artoo, stay with me. Freeze! You Rebel scum.', 'villaem-water-purifier', 18000000, 'poto_produk/1630242442_1fb48362c213f8d2822a.png', 2);
/*!40000 ALTER TABLE `tabel_product` ENABLE KEYS */;

-- Dumping structure for table mini-project.tabel_transaksi
CREATE TABLE IF NOT EXISTS `tabel_transaksi` (
  `transaksi_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=proses,2= sudah dikirim',
  `no_resi` varchar(20) NOT NULL,
  PRIMARY KEY (`transaksi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table mini-project.tabel_transaksi: ~9 rows (approximately)
/*!40000 ALTER TABLE `tabel_transaksi` DISABLE KEYS */;
INSERT IGNORE INTO `tabel_transaksi` (`transaksi_id`, `customer_id`, `tanggal`, `status`, `no_resi`) VALUES
	(1, 1, '2015-03-13', 2, 'smr0045565656'),
	(2, 1, '2015-04-29', 1, ''),
	(3, 1, '2015-04-29', 1, ''),
	(4, 1, '2015-04-29', 1, ''),
	(5, 1, '2015-04-29', 1, ''),
	(6, 1, '2015-04-29', 1, ''),
	(7, 2, '2015-04-29', 1, ''),
	(8, 2, '2015-04-29', 1, ''),
	(9, 6, '2015-04-29', 1, '');
/*!40000 ALTER TABLE `tabel_transaksi` ENABLE KEYS */;

-- Dumping structure for table mini-project.tabel_transaksi_detail
CREATE TABLE IF NOT EXISTS `tabel_transaksi_detail` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaksi_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Dumping data for table mini-project.tabel_transaksi_detail: ~6 rows (approximately)
/*!40000 ALTER TABLE `tabel_transaksi_detail` DISABLE KEYS */;
INSERT IGNORE INTO `tabel_transaksi_detail` (`detail_id`, `transaksi_id`, `product_id`, `qty`) VALUES
	(16, 9, 5, 6),
	(17, 9, 4, 6),
	(19, 9, 9, 6),
	(20, 9, 5, 5),
	(21, 9, 5, 1),
	(22, 9, 5, 1);
/*!40000 ALTER TABLE `tabel_transaksi_detail` ENABLE KEYS */;

-- Dumping structure for table mini-project.user
CREATE TABLE IF NOT EXISTS `user` (
  `id_users` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(250) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `salt` varchar(250) NOT NULL,
  `images` text NOT NULL,
  `id_user_level` int(11) NOT NULL,
  `is_aktif` enum('y','n') NOT NULL,
  PRIMARY KEY (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table mini-project.user: ~5 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT IGNORE INTO `user` (`id_users`, `email`, `username`, `password`, `full_name`, `salt`, `images`, `id_user_level`, `is_aktif`) VALUES
	(1, 'admin@gmail.com', 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', 'Admin', '', '', 1, 'y'),
	(3, 'user@gmail.com', 'user', '5f4dcc3b5aa765d61d8327deb882cf99', 'User', '', '', 2, 'y'),
	(4, 'jose@gmail.com', 'jose', 'd41d8cd98f00b204e9800998ecf8427e', 'Jose', '', '', 1, 'y'),
	(6, 'joekrisbel3@gmail.com', 'joekrisbel3', '6d768d6f7fe79f58b1a0c95f3eff0afd6127b0b02862d4.56385734', '', '6127b0b02862d4.56385734', '', 3, 'y'),
	(7, 'joekrisbel@gmail.com', 'joekrisbel', 'a3075222ec90aa63dbb10d8a053e62fe6128faaa27cac2.42572717', 'Yosef  Kribela Pandu Perwira', '6128faaa27cac2.42572717', '', 3, 'y');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table mini-project.user_level
CREATE TABLE IF NOT EXISTS `user_level` (
  `id_user_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(30) NOT NULL,
  PRIMARY KEY (`id_user_level`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table mini-project.user_level: ~6 rows (approximately)
/*!40000 ALTER TABLE `user_level` DISABLE KEYS */;
INSERT IGNORE INTO `user_level` (`id_user_level`, `nama_level`) VALUES
	(1, 'Admin'),
	(2, 'Accounts Payable User'),
	(3, 'Technician User'),
	(4, 'Sales User'),
	(5, 'Sales Admin Level 1 User'),
	(6, 'Sales Admin Level 2 User');
/*!40000 ALTER TABLE `user_level` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
