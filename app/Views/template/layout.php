<!DOCTYPE html>
<html>

    <?= $this->include('template/header') ?>
    <body>
        <div class="app">
            <div class="layout">
                <!-- Side Nav START -->
                <?= $this->include('template/menu') ?>
                <!-- Side Nav END -->

                <!-- Page Container START -->
                <div class="page-container">
                    <!-- Header START -->
                    <div class="header navbar">
                        <div class="header-container">
                            <ul class="nav-left">
                                <li>
                                    <a class="side-nav-toggle" href="javascript:void(0);">
                                        <i class="ti-view-grid"></i>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav-right">
                            <li class="user-profile dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown">
                                    <img class="profile-img img-fluid" src="assets/images/user.jpg" alt="">
                                    <div class="user-info">
                                        <span class="name pdd-right-5"><?php $session = session()?> <?php echo $session->get('username')?></span>
                                        <i class="ti-angle-down font-size-10"></i>
                                    </div>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- <li>
                                        <a href="">
                                            <i class="ti-settings pdd-right-10"></i>
                                            <span>Setting</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <i class="ti-user pdd-right-10"></i>
                                            <span>Profile</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <i class="ti-email pdd-right-10"></i>
                                            <span>Inbox</span>
                                        </a>
                                    </li>
                                    <li role="separator" class="divider"></li> -->
                                    <li>
                                        <a href="../auth/logout">
                                            <i class="ti-power-off pdd-right-10"></i>
                                            <span>Logout</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                                <li>
                                    <a class="side-panel-toggle" href="javascript:void(0);">
                                        <i class="ti-align-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Header END -->

                    <!-- Side Panel START -->
                    <div class="side-panel">
                        <div class="side-panel-wrapper bg-white">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#tab-1" role="tab" data-toggle="tab">
                                        <span>Tab1</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#tab-2" role="tab" data-toggle="tab">
                                        <span>Tab2</span>
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link" href="#tab-3" role="tab" data-toggle="tab">
                                        <span>Tab3</span>
                                    </a>
                                </li>
                                <li class="panel-close">
                                    <a class="side-panel-toggle" href="javascript:void(0);">
                                        <i class="ti-close"></i>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" role="tabpanel" class="tab-pane fade in active">
                                    <div class="padding-15">
                                        <!-- Content goes here -->
                                    </div>
                                </div>
                                <div id="tab-2" role="tabpanel" class="tab-pane fade">
                                    <div class="padding-15">
                                        <!-- Content goes here -->
                                    </div>
                                </div>
                                <div id="tab-3" role="tabpanel" class="tab-pane fade">
                                    <div class="padding-15">
                                        <!-- Content goes here -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Side Panel END -->

                    <!-- Content Wrapper START -->
                    <div class="main-content">
                        <div class="container-fluid">
                            <!-- Your content goes here -->
                            <?php echo $content; ?>
                        </div>
                    </div>
                    <!-- Content Wrapper END -->

                    <!-- Footer START -->
                    <footer class="content-footer">
                        <div class="footer">
                            <div class="copyright">
                                <span>Copyright © <?php echo date("Y").' r-' . $_ENV['RELEASE_VERSION'] ?></span>
                                <!-- <span class="go-right">
                                    <a href="" class="text-gray mrg-right-15">Link 1</a>
                                    <a href="" class="text-gray">Link 2</a>
                                </span> -->
                            </div>
                        </div>
                    </footer>
                    <!-- Footer END -->

                </div>
                <!-- Page Container END -->

            </div>
        </div>

        <!-- build:js assets/js/vendor.js -->
        <!-- plugins js -->
        <script src="<?php echo base_url();?>/public/assets/vendors/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url();?>/public/assets/vendors/popper.js/dist/umd/popper.min.js"></script>
        <script src="<?php echo base_url();?>/public/assets/vendors/bootstrap/dist/js/bootstrap.js"></script>
        <script src="<?php echo base_url();?>/public/assets/vendors/PACE/pace.min.js"></script>
        <script src="<?php echo base_url();?>/public/assets/vendors/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
        <!-- endbuild -->

        
    <!-- page plugins js -->
    <script src="<?php echo base_url();?>/public/assets/vendors/bower-jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo base_url();?>/public/assets/js/maps/jquery-jvectormap-us-aea.js"></script>
    <script src="<?php echo base_url();?>/public/assets/vendors/d3/d3.min.js"></script>
    <script src="<?php echo base_url();?>/public/assets/vendors/nvd3/build/nv.d3.min.js"></script>
    <script src="<?php echo base_url();?>/public/assets/vendors/jquery.sparkline/index.js"></script>
    <script src="<?php echo base_url();?>/public/assets/vendors/chart.js/dist/Chart.min.js"></script>
    <script src="<?php echo base_url();?>/public/assets/vendors/selectize/dist/js/standalone/selectize.min.js"></script>
    
    <script src="<?php echo base_url();?>/public/assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>/public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url();?>/public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>/public/assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script src="<?php echo base_url();?>/public/assets/vendors/summernote/dist/summernote.min.js"></script>
        <!-- build:js assets/js/app.min.js -->
        <!-- core js -->
        <script src="<?php echo base_url();?>/public/assets/js/app.js"></script>
        <!-- endbuild -->
 <!-- page plugins js -->
 <!-- <script src="<?php echo base_url();?>/public/assets/vendors/datatables/media/js/jquery.dataTables.js"></script>
 <script src="<?php echo base_url();?>/public/assets/vendors/datatables/media/js/export_tabel/jquery.dataTables.min.js"></script>
 <script src="<?php echo base_url();?>/public/assets/vendors/datatables/media/js/export_tabel/dataTables.buttons.min.js"></script>
 <script src="<?php echo base_url();?>/public/assets/vendors/datatables/media/js/export_tabel/buttons.flash.min.js"></script>
 <script src="<?php echo base_url();?>/public/assets/vendors/datatables/media/js/export_tabel/jszip.min.js"></script>
 <script src="<?php echo base_url();?>/public/assets/vendors/datatables/media/js/export_tabel/pdfmake.min.js"></script>
 <script src="<?php echo base_url();?>/public/assets/vendors/datatables/media/js/export_tabel/vfs_fonts.js"></script>
 <script src="<?php echo base_url();?>/public/assets/vendors/datatables/media/js/export_tabel/buttons.html5.min.js"></script>
 <script src="<?php echo base_url();?>/public/assets/vendors/datatables/media/js/export_tabel/buttons.print.min.js"></script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>

<!-- page js -->
<!-- <script src="<?php echo base_url();?>/public/assets/js/table/data-table.js"></script> -->
        <!-- Insert your dependencies here -->
        <!-- <script src="<?php echo base_url();?>/public/assets/js/dashboard/dashboard.js"></script> -->
        <script src="<?php echo base_url();?>/public/assets/js/charts/chartjs.js"></script>
        <!-- <script src="https://cdn.jsdelivr.net/gh/emn178/chartjs-plugin-labels/src/chartjs-plugin-labels.js"></script> -->
        <script src="<?php echo base_url();?>/public/assets/js/miniprojek/produk.js"></script> 
    </body>
</html>