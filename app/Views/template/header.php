<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
    <title>Project Mini</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>/public/assets/images/logo/favicon.png">

    <!-- plugins css -->
    <link rel="stylesheet" href="<?php echo base_url();?>/public/assets/vendors/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/public/assets/vendors/PACE/themes/blue/pace-theme-minimal.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/public/assets/vendors/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <!-- page plugins css -->

    <!-- page plugins css -->
    <link rel="stylesheet" href="<?php echo base_url();?>/public/assets/vendors/selectize/dist/css/selectize.default.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>/public/assets/vendors/summernote/dist/summernote.css" />

    <!-- core css -->
    <link href="<?php echo base_url();?>/public/assets/css/ei-icon.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/public/assets/css/themify-icons.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/public/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/public/assets/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/public/assets/css/app.css" rel="stylesheet">

    <!-- datatables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.22/datatables.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/assets/vendors/datatables/media/js/export_tabel/jquery.dataTables.min.css">
    <script type="text/javascript" src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/assets/vendors/datatables/media/js/export_tabel/buttons.dataTables.min.css">
    
</head>