<div class="side-nav">
                    <div class="side-nav-inner">
                        <div class="side-nav-logo">
                            <a href="index.html">
                                <div class="logo logo-dark" style="background-image: url('<?php echo base_url();?>/public/assets/images/logo/logo.png')"></div>
                                <!-- <div class="logo logo-dark" style="background-image: url('<?php echo base_url();?>/public/assets/images/logo/logo.png')"></div> -->
                                <div class="logo logo-white" style="background-image: url('<?php echo base_url();?>/public/assets/images/logo/logo-white.png')"></div>
                            </a>
                            <div class="mobile-toggle side-nav-toggle">
                                <a href="">
                                    <i class="ti-arrow-circle-left"></i>
                                </a>
                            </div>
                        </div>
                        <ul class="side-nav-menu scrollable">
                            <li class="nav-item">
                                <a class="" href="../admin/dashboard">
                                    <span class="icon-holder">
                                        <i class="ti-home"></i>
                                    </span>
                                    <span class="title">Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="" href="../admin/produk">
                                    <span class="icon-holder">
                                    <i class="ti-package"></i>
                                    </span>
                                    <span class="title">Produk</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="" href="../admin/dashboard">
                                    <span class="icon-holder">
                                        <i class="ei-office-cart"></i>
                                    </span>
                                    <span class="title">Sales</span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="javascript:void(0);">
                                    <span class="icon-holder">
                                        <i class="ti-layout-media-overlay"></i>
                                    </span>
                                    <span class="title">Master Data</span>
                                    <span class="arrow">
                                        <i class="ti-angle-right"></i>
                                    </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item dropdown">
                                        <a href="../admin/diskon">
                                            <span>Diskon </span>
                                        </a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a href="../admin/kategori-produk">
                                            <span>Kategori Produk</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                         
                        </ul>
                    </div>
                </div>