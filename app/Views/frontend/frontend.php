<!DOCTYPE html>
<html>

<?= $this->include('template/header') ?>

<body>
    <!-- Header START -->
    <?= $this->include('frontend/template/header_navbar') ?>
    <!-- Header END -->

    <!-- Content Wrapper START -->
    <div class="main-content">
        <div class="container-fluid">
            <div class="page-title">
                <h4>Home</h4>
            </div>
            <div class="row">
            <?php foreach($dataProduk as $datas): ?>
           
                <div class="card col-sm-3">
                    <div class="card-media">
                        <img class="img-responsive" src="<?= base_url($_ENV['file_path'].$datas['gambar'])?>"  alt="">
                    </div>
                    <div class="card-block">
                        <h4 class="mrg-btm-20 no-mrg-top"><?= $datas['nama_product'] ?></h4>

                        <p><?= $datas['discription_product'] ?></p>
                        <div class="mrg-top-40">
                            <a href="<?= $datas['product_id'] ?>" class="btn btn-info no-mrg-btm">Beli</a>
                        </div>
                    </div>
                </div>
            
            <?php endforeach ?>
            </div>
            <!-- Modal END-->
        </div>
        </div>
        </div>
        <!-- Content Wrapper END -->

        <!-- plugins js -->
        <?= $this->include('frontend/template/lib_js') ?>
        <!-- endbuild -->

        <!-- build:js assets/js/app.min.js -->
        <!-- core js -->
        <script src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/js/app.js">
        </script>
        <!-- endbuild -->

</body>

</html>