<!DOCTYPE html>
<html>

<?= $this->include('template/header') ?>

<body>
    <!-- Header START -->
    <?= $this->include('frontend/template/header_navbar') ?>
    <!-- Header END -->

    <!-- Content Wrapper START -->
    <div class="main-content">
        <div class="card">
            <div class="card-heading border bottom">
                <h4 class="card-title">Profil</h4>
            </div>
            <div class="card-block">
                <div class="card-block">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="widget-profile-1 card">
                                <div class="profile border bottom">
                                    <img width="80" height="80" class="mrg-top-30"
                                        src="<?= base_url($_ENV['file_path'].$data_profil['images'])?>" alt="">
                                    <h4 class="mrg-top-20 no-mrg-btm text-semibold"><?= $data_profil['full_name'] ?>
                                    </h4>
                                    <p></p>
                                </div>
                                <div class="pdd-horizon-30 pdd-vertical-20">
                                    <h6 class="text-semibold mrg-btm-5">Type Customer
                                        :<?= $data_profil['customer_type'] ?></h6>

                                    <div class="mrg-top-30 text-center">
                                        <ul class="list-unstyled list-inline">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="card">
                                <div class="card-heading border bottom">
                                    <h4 class="card-title">General Info</h4>
                                </div>
                                <div class="card-block">
                                    <?php 
                                                            $session = session();
                                                            $update_profil = $session->getFlashdata('update_profil');
                                                            $error = $session->getFlashdata('error');
                                                        ?>
                                    <?php if($error){ ?>
                                    <div class='alert alert-danger mt-2'>
                                        <p style="color:red">Terjadi Kesalahan:
                                            <ul>
                                                <?php foreach($error as $e){ ?>
                                                <li style="color:red"><?php echo $e ?></li>
                                                <?php } ?>
                                            </ul>
                                        </p>
                                    </div>

                                    <?php } ?>
                                    <?php if($update_profil){ ?>
                                                        <p style="color:green"><?php echo $update_profil?></p>
                                                    <?php } ?>
                                    <div class="row">

                                        <div class="col-md-3">
                                            <p class="mrg-top-10 text-dark"> <b>Foto</b></p>
                                        </div>

                                        <form method="post" enctype="multipart/form-data"
                                            action="<?php echo base_url('update_profil');?>">
                                            <div class="col-md-6">
                                                <div>
                                                    <label for="img-upload" class="pointer">
                                                        <img src="<?= base_url($_ENV['file_path'].$data_profil['images'])?>"
                                                            width="50" alt="">
                                                        <span class="btn btn-default display-block no-mrg-btm">Choose
                                                            file</span>
                                                        <input class="d-none mrg-top-10 " type="file" name="images"
                                                            multiple id="img-upload">
                                                    </label>
                                                </div>
                                            </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="mrg-top-10 text-dark"> <b>Name</b></p>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="name" class="form-control"
                                                value="<?= $data_profil['full_name'] ?>">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="mrg-top-10 text-dark"> <b>Jenis
                                                    Kelamin<?= $data_profil['jenis_kelamin']?></b></p>
                                        </div>
                                        <div class="col-md-6">
                                            <select id="selectize-dropdown" name="jenis_kelamin" class="form-control">
                                                <option value="">- Pilih Jenis Kelamin -</option>
                                                <option
                                                    <?= $data_profil['jenis_kelamin']=='laki-laki' ? 'selected="selected"' : ''?>
                                                    value="laki-laki">Laki-laki</option>
                                                <option
                                                    <?= $data_profil['jenis_kelamin']=='perempuan' ? 'selected="selected"' : '' ?>
                                                    value="perempuan">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="mrg-top-10 text-dark"> <b>Tanggal Lahir</b></p>
                                        </div>
                                        <div class="col-md-6">

                                            <div class="timepicker-input input-group">
                                                <span class="input-group-text">
                                                    <i class="ti-calendar"></i>
                                                </span>
                                                <input type="text" name='tanggal_lahir'
                                                    value="<?= date("d-m-Y", strtotime($data_profil['tanggal_lahir'])); ?>"
                                                    class="form-control datepicker-2" placeholder="Pick your date"
                                                    data-provide="datepicker">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="mrg-top-10 text-dark"> <b>Nomor HP</b></p>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="nomor_hp" class="form-control"
                                                value="<?= $data_profil['nomor_hp'] ?>">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="mrg-top-10 text-dark"> <b>Email</b></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p class="mrg-top-10"><?= $data_profil['email'] ?></p>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <p class="mrg-top-10 text-dark"> <b>Status</b></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p class="mrg-top-10">
                                                <span
                                                    class="status <?= $data_profil['is_aktif'] == 'y' ? 'online' : 'offline' ?> mrg-top-10"></span>
                                                <span
                                                    class="mrg-left-10"><?= $data_profil['is_aktif'] == 'y' ? 'Active' : 'InActive' ?></span>
                                            </p>
                                        </div>
                                    </div>
                                    <hr>
                                    <!-- 
                                    <hr> -->
                                    <div class="row">
                                        <button type="submit" class="btn btn-info">Simpan</button>
                                    </div>
                                    </form>

                                    <hr>
                                <div class="row">
                                        <div class="col-md-3">
                                        <p class="mrg-top-10 text-dark"> <b>New Password</b></p>
                                    </div>
                                    <div class="col-md-6">
                                        <button data-toggle="modal" data-target="#modal-sm"
                                            class="btn btn-sm btn-primary">Ubah</button>
                                    </div>
                                    <div class="modal fade" id="modal-sm">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Password</label>
                                                            <form method="post" action="<?php echo base_url('update_profil');?>">
                                                            <input type="password" name='password' placeholder="Password"
                                                                class="form-control">
                                                                <button type="submit" class="btn btn-info">Simpan</button>
                                                            </form>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <button data-dismiss="modal"
                                                    class="btn btn-primary btn-block no-mrg no-border pdd-vertical-15 ng-scope">
                                                    <span class="text-uppercase">Close</span>
                                                </button>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>

                               
                               

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal END-->
    </div>
    <!-- Content Wrapper END -->

    <!-- plugins js -->
    <?= $this->include('frontend/template/lib_js') ?>
    <!-- endbuild -->

    <!-- build:js assets/js/app.min.js -->
    <!-- core js -->
    <script
        src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js">
    </script>
    <script src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/js/app.js"></script>
    <script
        src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/vendors/bootstrap-daterangepicker/daterangepicker.js">
    </script>
    <script
        src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/js/miniprojek/profil.js">
    </script>
    <script
        src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js">
    </script>
    <script>



    </script>
    <!-- endbuild -->

</body>

</html>