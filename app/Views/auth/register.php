<!-- <!DOCTYPE html>
<html> -->

<?= $this->include('template/header') ?>

<body>
    <div class="app">
        <div class="authentication">
            <div class="sign-in-2">
                <div class="container-fluid no-pdd-horizon bg" style="background-image: url('<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/images/others/img-30.jpg')">
                    <div class="row">
                        <div class="col-md-10 mr-auto ml-auto">
                            <div class="row">
                                <div class="mr-auto ml-auto full-height height-100 d-flex align-items-center">
                                    <div class="vertical-align full-height">
                                        <div class="table-cell">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="pdd-horizon-30 pdd-vertical-30">
                                                    <h5>Register</h5>
                                                        <?php 
                                                            $session = session();
                                                            $error = $session->getFlashdata('error');
                                                        ?>
                                                        <?php if($error){ ?>
                                                            <div class='alert alert-danger mt-2'>
                                                                <p style="color:red">Terjadi Kesalahan:
                                                                    <ul>
                                                                        <?php foreach($error as $e){ ?>
                                                                        <li style="color:red"><?php echo $e ?></li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </p>
                                                            </div>
                                                       
                                                        <?php } ?>
                                                     
                                                        <p class="mrg-btm-15 font-size-13">Please enter your name, username and password to register</p>
                                                        <form method="post" action="<?php echo base_url();?>/auth/valid_register">
                                                                    Name: <br>
                                                                    <input type="text" class="form-control" name="name" required><br>
                                                                    Email: <br>
                                                                    <input type="email" class="form-control" name="email" required><br>
                                                                    Username: <br>
                                                                    <input type="text" class="form-control" name="username" required><br>
                                                                    Password: <br>
                                                                    <input type="password" class="form-control" name="password" required><br>
                                                                    Konfirmasi Password: <br>
                                                                    <input type="password" class="form-control" name="confirm" required><br>
                                                                    <!-- <button type="submit" class="btn btn-info" name="login">Register</button> -->
                                                                
                                                                <p>
                                                                    <a href="<?php echo base_url();?>/auth/login">Sudah punya akun?</a>
                                                                </p>
                                                       
                                                            <!-- <div class="pull-right">
                                                                <a href="">Forgot Password?</a>
                                                            </div> -->
                                                            <div class="mrg-top-20 text-right">
                                                                <button type="submit" class="btn btn-info">Register</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- build:js assets/js/vendor.js -->
    <!-- plugins js -->
    <script src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/vendors/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/vendors/bootstrap/dist/js/bootstrap.js"></script>
    <script src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/vendors/PACE/pace.min.js"></script>
    <script src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/vendors/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
    <!-- endbuild -->

    <!-- build:js assets/js/app.min.js -->
    <!-- core js -->
    <script src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/js/app.js"></script>
    <!-- endbuild -->

    <!-- page js -->

</body>

<!-- </html> -->