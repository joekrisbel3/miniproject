<!-- <!DOCTYPE html>
<html> -->

<?= $this->include('template/header') ?>

<body>
    <div class="app">
        <div class="authentication">
            <div class="sign-in-2">
                <div class="container-fluid no-pdd-horizon bg" style="background-image: url('<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/images/others/img-30.jpg')">
                    <div class="row">
                        <div class="col-md-10 mr-auto ml-auto">
                            <div class="row">
                                <div class="mr-auto ml-auto full-height height-100 d-flex align-items-center">
                                    <div class="vertical-align full-height">
                                        <div class="table-cell">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="pdd-horizon-30 pdd-vertical-30">
                                                    <?php 
                                                        $session = session();
                                                        $login = $session->getFlashdata('login');
                                                        $username = $session->getFlashdata('username');
                                                        $password = $session->getFlashdata('password');
                                                    ?>
                                                    
                                                    <h5>Login</h5>
                                                    
                                                    <?php if($username){ ?>
                                                        <p style="color:red"><?php echo $username?></p>
                                                    <?php } ?>
                                                    
                                                    <?php if($password){ ?>
                                                        <p style="color:red"><?php echo $password?></p>
                                                    <?php } ?>
                                                    
                                                    <?php if($login){ ?>
                                                        <p style="color:green"><?php echo $login?></p>
                                                    <?php } ?>
                                                        <!-- <div class="mrg-btm-30">
                                                            <h2 class="inline-block pull-right no-mrg-vertical pdd-top-15">Login</h2>
                                                        </div> -->
                                                        <p class="mrg-btm-15 font-size-13">Please enter your user name and password to login</p>
                                                        <form class="kt-form" method="post" action="<?php echo base_url();?>/auth/valid_login">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="username" placeholder="User name">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="password" class="form-control" name="password" placeholder="Password">
                                                            </div>
                                                            <div class="checkbox font-size-13 inline-block no-mrg-vertical no-pdd-vertical">
                                                                <!-- <input id="agreement" name="agreement" type="checkbox">
                                                                <label for="agreement">Keep Me Signed In</label> -->
                                                                <p>
                                                                    <a href="<?php echo base_url();?>/auth/register">Belum punya akun?</a>
                                                                </p>
                                                            </div>
                                                            <!-- <div class="pull-right">
                                                                <a href="">Forgot Password?</a>
                                                            </div> -->
                                                            <div class="mrg-top-20 text-right">
                                                                <button type="submit" class="btn btn-info">Login</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- build:js assets/js/vendor.js -->
    <!-- plugins js -->
    <script src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/vendors/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/vendors/bootstrap/dist/js/bootstrap.js"></script>
    <script src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/vendors/PACE/pace.min.js"></script>
    <script src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/vendors/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
    <!-- endbuild -->

    <!-- build:js assets/js/app.min.js -->
    <!-- core js -->
    <script src="<?php echo base_url();echo $_ENV['status_dev']=='server' ? '' : '/public'?>/assets/js/app.js"></script>
    <!-- endbuild -->

    <!-- page js -->

</body>

<!-- </html> -->