
<input id="base_url" type="hidden" value="<?= base_url(); ?>" />
<input id="tanggal" type="hidden" value="<?= date('d-m-Y')?>" />
<div class="row">
    <div class="col-sm-12">
        <!-- <div class="card">
            <div class="card-block"> -->
        <div class="form-group row">
            <div class="col-md-2">
                <div class="form-group">
                    <select type="text" class="form-control" id="tipe_view" name="tipe_view">
                        <option value="bulanan" selected>Bulanan</option>
                        <option value="periode">Periode</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="timepicker-input input-icon form-group">
                    <i class="ti-time"></i>

                    <input type="text" id="datepicker" class="form-control" name="tanggal_dashboard" autocomplete="off"
                        value="" placeholder="Bulan Tahun">
                    <input type="hidden" id="datepicker_start" class="form-control" name="mulai_tanggal_dashboard"
                        autocomplete="off" value="" placeholder="Tanggal Mulai" data-provide="datepicker" data-date-format='dd-mm-yyyy'>
                </div>
            </div>

            <div class="col-md-2 datepicker_end_class">
                <div class="timepicker-input input-icon form-group">
                    <i class="ti-time"></i>
                    <input type="hidden" id="datepicker_end" class="form-control" name="selesai_tanggal_dashboard"
                        autocomplete="off" value="" placeholder="Tanggal Akhir" data-provide="datepicker" data-date-format='dd-mm-yyyy'>
                </div>
            </div>

            <div class="col-md-2">
                <button type="button" class="btn btn-info" id="searchDashboard">
                Search <img src="<?= base_url('/public/assets/images/tenor.gif'); ?>" id="loading" width="15" height="15" alt="Italian Trulli"></button>
            </div>
        </div>
        <!-- </div>
        </div> -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-heading border bottom">
                <h5 class="card-title">Jumlah Penjualan</h5>
                <h6 id="judul-jml-kun" class="card-title"></h6>
            </div>
            <div class="card-block">
                <div>
                    <canvas id="line-chart"></canvas>
                </div>
            </div>
        </div>
    </div>

</div>