
       

<div class="row">
    <div class="modal fade" id="modal-lg-edit">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4 class="card-title">Edit Produk</h4>
                    <div class="row">
                        <div class="col-md-12 ml-auto mr-auto">
                        
                        <input type="hidden" id="getKaregoriProduk" value="<?= base_url('/admin/produk/getKaregoriProduk') ?>">
                            <form method="post" enctype="multipart/form-data" action="<?php echo base_url('/admin/produk/store_produk');?>">
                                <input type="hidden" id="idProduk" name="product_id" value="">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nama Product</label>
                                            <input type="text" placeholder="Nama Product" name="nama_product"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nama Product Seo</label>
                                            <input type="text" placeholder="Nama Product Seo" name="nama_product_seo"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Harga</label>
                                            <input type="text" placeholder="Harga" name="harga" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Gambar</label>
                                            <input type="file" placeholder="Gambar"  multiple name="gambar" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Kategori Produk</label>
                                            <select type="select" id="kategori_id_edit" name="kategori_id" class="form-control kt-select2">
                                                <!-- <option value="">-- Pilih COB --</option> -->
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <button data-dismiss="modal"
                    class="btn btn-primary btn-block no-mrg no-border pdd-vertical-15 ng-scope">
                    <span class="text-uppercase">Close</span>
                </button>

            </div>
        </div>
    </div>
</div>