 <!-- Content Wrapper START -->
 <style>
     body {
         font-family: Roboto, -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", "Helvetica Neue", Arial, sans-serif;
         font-size: 14px;
         background-color: #f6f7fb;
         color: #000000;
         line-height: 1.5;
         letter-spacing: 0.2px;
         overflow-x: hidden;
     }
 </style>
 <div class="page-title">
     <h4>Produk</h4>
 </div>
 <div class="row">
     <div class="col-md-12">
         <div class="card">
             <div class="card-block">
                 <?= $this->include('admin/produk/produk_add') ?>
                 <?php  
                        $session = session();
                        $produk = $session->getFlashdata('produk'); 
                        
                        if($produk){ ?>
                 <div class='alert alert-success mt-2'>
                     <p style="color:green"><?php echo $produk?></p>
                 </div>
                 <?php } ?>
                 <div class="table-overflow">
                     <table class="table">
                         <thead>
                             <tr>
                                 <th>No</th>
                                 <th>Nama Product</th>
                                 <th>Kategori</th>
                                 <th>Harga</th>
                                 <th>Gambar</th>
                                 <th>Seo</th>
                                 <th>Action</th>
                             </tr>
                         </thead>
                         <tbody>
                             <?php foreach($dataProduk as $datas): ?>
                             <tr>
                                 <td><?= $datas['product_id'] ?></td>
                                 <td>
                                     <?= $datas['nama_product'] ?>
                                 </td>
                                 <td>
                                 <?= $datas['nama_kategori'] ?>
                                    
                                 </td>
                                 <td>
                                     <?= $datas['harga'] ?>
                                 </td>
                                 <td>
                                 <?= $datas['nama_product_seo'] ?>
                                 </td>
                                 <td>
                                 <img src="<?= base_url($_ENV['file_path'].$datas['gambar'])?>" width="50" alt="">
                                 </td>
                                 <td>
                                 <button name="editId" data-toggle="modal" 
                                        data-id="<?= $datas['product_id']?>" 
                                        data-namaproduct="<?= $datas['nama_product']?>" 
                                        data-kategoriid="<?= $datas['kategori_id']?>" 
                                        data-harga="<?= $datas['harga']?>" 
                                        data-namaproductseo="<?= $datas['nama_product_seo']?>" 
                                        data-gambar="<?= $datas['gambar']?>" 
                                 
                                 
                                 data-target="#modal-lg-edit" class="btn btn-sm btn-primary">Edit</button>
                                     <a href="<?= base_url('admin/produk/delete/'.$datas['product_id']) ?>"
                                         onclick="confirmToDelete(this)"
                                         class="btn btn-sm btn-outline-danger">Delete</a>
                                 </td>
                             </tr>
                             <?php endforeach ?>
                         </tbody>
                     </table>
                 </div>
             </div>
         </div>
     </div>
     <?= $this->include('admin/produk/produk_edit') ?>
 </div>
 


     <!-- Content Wrapper END -->