<?php
 
if ( ! function_exists('render'))
{
    function render(string $name, array $data = [], array $options = [])
    {
        return view(
            'template/layout',
            [
                'content' => view($name, $data, $options),
            ],
            $options
        );
    }
}