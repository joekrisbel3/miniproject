<?php namespace App\Models;
use CodeIgniter\Model;


class ProdukModel extends Model
{
    protected $table      = 'tabel_product';
    protected $primaryKey = 'product_id';
    protected $useAutoIncrement = true;
    protected $allowedFields = [
        'nama_product',
        'discription_product',
        'nama_product_seo',
        'harga',
        'gambar',
        'kategori_id'
    ]; 

}