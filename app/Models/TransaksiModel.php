<?php namespace App\Models;
use CodeIgniter\Model;


class TransaksiModel extends Model
{
    protected $table      = 'tabel_transaksi';
    protected $primaryKey = 'transaksi_id';
    protected $useAutoIncrement = true;
    protected $allowedFields = [
        'customer_id',
        'tanggal',
        'status',
        'no_resi'
    ]; 
}

