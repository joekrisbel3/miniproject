<?php namespace App\Models;
use CodeIgniter\Model;


class TransaksiDetailModel extends Model
{
    protected $table      = 'tabel_transaksi_detail';
    protected $primaryKey = 'detail_id';
    protected $useAutoIncrement = true;
    protected $allowedFields = [
        'transaksi_id',
        'product_id',
        'qty'
    ]; 
}



