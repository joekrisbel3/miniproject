<?php namespace App\Models;
use CodeIgniter\Model;


class UserLevelModel extends Model
{
    protected $table      = 'user_level';
    protected $primaryKey = 'id_user_level';
    protected $useAutoIncrement = true;
    protected $allowedFields = [
        'nama_level'
    ]; 
}

