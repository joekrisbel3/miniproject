<?php namespace App\Models;
use CodeIgniter\Model;


class CustomerModel extends Model
{
    protected $table      = 'customer';
    protected $primaryKey = 'id_users';
    protected $useAutoIncrement = true;
    protected $allowedFields = [
        'username',
        'full_name',
        'tanggal_lahir',
        'jenis_kelamin',
        'email',
        'password',
        'nomor_hp',
        'salt',
        'images',
        'customer_type',
        'is_aktif'
    ]; 


}

