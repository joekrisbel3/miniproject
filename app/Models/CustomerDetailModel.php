<?php namespace App\Models;
use CodeIgniter\Model;


class CustomerDetailModel extends Model
{
    protected $table      = 'customer_detail';
    protected $primaryKey = 'customer_detail_id';
    protected $useAutoIncrement = true;
    protected $allowedFields = [
        'customer_alamat'
    ]; 
}

