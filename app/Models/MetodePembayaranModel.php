<?php namespace App\Models;
use CodeIgniter\Model;


class MetodePembayaranModel extends Model
{
    protected $table      = 'tabel_metode_pembayaran';
    protected $primaryKey = 'metode_id';
    protected $useAutoIncrement = true;
    protected $allowedFields = [
        'metode_nama',
        'metode_jumlah_bulan'
    ]; 
}


