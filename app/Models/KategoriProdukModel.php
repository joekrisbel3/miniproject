<?php namespace App\Models;
use CodeIgniter\Model;


class KategoriProdukModel extends Model
{
    protected $table      = 'tabel_kategori';
    protected $primaryKey = 'kategori_id';
    protected $useAutoIncrement = true;
    protected $allowedFields = [
        'nama_kategori',
        'link',
        'parent',
        'nama_kategori_seo'
    ]; 

}
