<?php namespace App\Controllers\Frontend;
use CodeIgniter\Controller;
use App\Models\CustomerModel;

class Profil extends Controller
{
	public function __construct()
    {
        //membuat user model untuk konek ke database 
        $this->customerModel = new CustomerModel();
        
        //meload validation
        $this->validation = \Config\Services::validation();
        
        //meload session
        $this->session = \Config\Services::session();
        
    }
	public function index()
	{
		if ($this->session->get("isLogin") != true ) {
			return redirect()->to(base_url('auth/login'));
		}else{
			$this->customer_id = $this->session->get('customer_id');
		}
		$data['data_profil'] =  $this->customerModel->where('id_users', $this->customer_id)->first();
		return view('frontend/profil',$data);
	}

	public function store_update()
    {

         //tangkap data dari form 
		 $data = $this->request->getPost();
		//  
		//  dd( $data->getFile('images'));
		
		// dd(isset($data['password']) == true);
		$this->customer_id = $this->session->get('customer_id');
		$data_edit =  $this->customerModel->where('id_users', $this->customer_id)->first();
		 if(isset($data['password'])){
			$salt = uniqid('', true);
			//hash password digabung dengan salt
			$password = md5($data['password']).$salt;
			$data_update['password'] = $password;
			$data_update['salt'] = $salt;

		 }else{
			//jalankan validasi
			$this->validation->run($data, 'update_profil');
		 
			//cek errornya
			$errors = $this->validation->getErrors();
			
			//jika ada error kembalikan ke halaman register
			if($errors){
				session()->setFlashdata('error', $errors);
				return redirect()->to(base_url('profil'));
			}

			$avatar = $this->request->getFile('images');
			//  dd(isset($avatar));
			$filelok = $_ENV['file_path'].$data_edit['images'];
			// dd($avatar->getError());
			 if($avatar->getError()==0){
				// dd('jalan ga');
				 if(file_exists($filelok) == true){
					unlink($_ENV['file_path'].$data_edit['images']);
				 }
				 $namaFoto = $avatar->getRandomName();
				 $avatar->move($_ENV['file_path'].'/poto_profil',$namaFoto);
				 $data_update['images'] = 'poto_profil/'.$namaFoto;
			 }	
			
			 $data_update['tanggal_lahir'] = date("Y-m-d", strtotime($data['tanggal_lahir']));
			 $data_update['full_name'] =  $data['name'];
			 $data_update['jenis_kelamin'] = $data['jenis_kelamin'];
			 $data_update['nomor_hp'] = $data['nomor_hp'];
		 }
		 
		 
		 //masukan data ke database
		 $this->customerModel->update($this->customer_id,$data_update);
		 
		 //arahkan ke halaman login
		 session()->setFlashdata('update_profil', 'Update profil berhasil');
		 return redirect()->to(base_url('profil'));

    }

}
