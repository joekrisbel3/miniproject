<?php namespace App\Controllers\Frontend;
use CodeIgniter\Controller;
use App\Models\ProdukModel;
use App\Models\KategoriProdukModel;

class Home extends Controller
{
	public function __construct()
    {
		helper('form');
		helper('date');
		$this->validation = \Config\Services::validation();
		$this->session = session();
		$this->produkModel = new ProdukModel();
		
	}
	public function index()
	{
		
		$data['dataProduk'] = 
		$this->produkModel
		->join('tabel_kategori', 'tabel_kategori.kategori_id = tabel_product.kategori_id', 'left')
		->findAll();
		return view('frontend/frontend',$data);
	}

	//--------------------------------------------------------------------

}
