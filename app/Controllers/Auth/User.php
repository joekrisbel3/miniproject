<?php
namespace App\Controllers\Auth;
use CodeIgniter\Controller;

class User extends Controller
{
    public function __construct()
    {
        $this->session = session();
    }
    
    public function index()
    {
        //cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/auth/login');
        }

        return view('user/index');
    }
    
}