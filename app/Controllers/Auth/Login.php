<?php namespace App\Controllers\Auth;
use CodeIgniter\Controller;
class Login extends Controller
{

	  public function __construct()
    {
        helper('form');
		$this->validation = \Config\Services::validation();
		$this->db = \Config\Database::connect();
		$this->session = session();
		$this->UserName = $this->session->get('UserName');

    }
	public function index()
	{
		$data['message'] = '';
		$data['cekValidasiUser']='';
		$data[''] = $this->UserName;
		return view('admin/auth/login', $data);
	}

	public function login()
    {
		$data = $this->request->getPost();
			$validate = $this->validation->run($data, 'login');
			$errors = $this->validation->getErrors();
			// dd($errors);
		$username   = $this->request->getPost('username');
		$password   = $this->request->getPost('password');

		// $queryUsername = $this->db->query("select aes_encrypt('$username','nur') AS username");
		// $queryPassword = $this->db->query("select aes_encrypt('$password','windi') AS `password`");
		// dd(	$queryUsername->getFirstRow()->username == 0x71BD59C49927E295C985A38DC5B8CA65,$queryPassword->getFirstRow()->password);
	
		$queryCekUserPass=	$this->model->queryCekUserPass($username,$password);
	if(	 $queryCekUserPass->cekUserPass == 1){
		$session_data = array(
			'cekUserPass' => $queryCekUserPass->cekUserPass,
			'usere'          =>  $username,
			'UserIP'            => $_SERVER['REMOTE_ADDR'],
		);
		$this->session->set($session_data);
		return redirect()->to(base_url('admin/dashboard'));
	}else{
		$data['message'] = $errors;
		$data['cekValidasiUser'] = 'Username Atau Password Salah';
		return view('admin/auth/login', $data);
	}
					// return view('admin/va/search', $data);
                
     
  
    }

    public function logout()
    {
		$this->session->destroy();
		return redirect()->to(base_url('auth/login'));
	}


}
