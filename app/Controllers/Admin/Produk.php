<?php namespace App\Controllers\Admin;
use CodeIgniter\Controller;
use App\Models\DatatabelsModel;
use App\Models\ProdukModel;
use App\Models\KategoriProdukModel;
use CodeIgniter\I18n\Time;

class Produk extends Controller
{
	public function __construct()
    {
		helper('form');
		helper('date');
		$this->validation = \Config\Services::validation();
		$this->session = session();
		$this->DatatabelsModel = new DatatabelsModel();
		$this->produkModel = new ProdukModel();
		
	}
	public function index()
	{
		 //cek apakah ada session bernama isLogin
		 if (($this->session->get("isLogin") != true) && ($this->session->get("id_user_level") == 1)) {
			return redirect()->to(base_url('auth/login'));
		}else{
			$this->customer_id = $this->session->get('customer_id');
		}
	
		$data['dataProduk'] = 
		$this->produkModel
		->join('tabel_kategori', 'tabel_kategori.kategori_id = tabel_product.kategori_id', 'left')
		->findAll();
		
		return render('admin/produk/produk_list',$data);    
	}

    public function store_produk()
    {
        

        $data_edit = $this->request->getPost();
		$avatar = $this->request->getFile('gambar');
		
			 if($avatar->getError()==0){
				 $namaFoto = $avatar->getRandomName();
				 $avatar->move($_ENV['file_path'].'/poto_produk',$namaFoto);
				 $data_update['gambar'] = 'poto_produk/'.$namaFoto;
			 }	
        
			 $data_update['nama_product'] = $data_edit['nama_product'];
			 $data_update['nama_product_seo'] =  $data_edit['nama_product_seo'];
			 $data_update['harga'] = $data_edit['harga'];
			 $data_update['kategori_id'] = $data_edit['kategori_id'];
        $this->produkModel->save($data_update);
        
       
        session()->setFlashdata('produk', 'Berhasil masukan data');
        return redirect()->to(base_url('admin/produk'));
    }


	

	public function getKaregoriProduk()
    {
        $model = new KategoriProdukModel();
		$getKaregoriProduk = $model->findAll();
        return json_encode($getKaregoriProduk);
	}

	public function store_edit_produk()
    {
		$produkModel = new ProdukModel();

        $data = $this->request->getPost();


		$avatar = $this->request->getFile('gambar');
			 if($avatar->getError()==0){
				$data_edit =  $produkModel->where('product_id', $data['product_id'])->first();
		$filelok = $_ENV['file_path'].$data_edit['gambar'];
		if(file_exists($filelok) == true){
				unlink($_ENV['file_path'].$data_edit['gambar']);
			 }
				 $namaFoto = $avatar->getRandomName();
				 $avatar->move($_ENV['file_path'].'/poto_produk',$namaFoto);
				 $data_update['gambar'] = 'poto_produk/'.$namaFoto;
			 }	
        
			 $data_update['nama_product'] = $data_edit['nama_product'];
			 $data_update['nama_product_seo'] =  $data_edit['nama_product_seo'];
			 $data_update['harga'] = $data_edit['harga'];
			 $data_update['kategori_id'] = $data_edit['kategori_id'];
        $this->produkModel->update($data['product_id'],$data_update);

        session()->setFlashdata('produk', 'Berhasil masukan data');
        return redirect()->to(base_url('admin/produk'));
    }

	public function delete($id){
        $produkModel = new ProdukModel();
		$data_edit =  $produkModel->where('product_id', $id)->first();
		$filelok = $_ENV['file_path'].$data_edit['gambar'];
		if(file_exists($filelok) == true){
				unlink($_ENV['file_path'].$data_edit['gambar']);
			 }
        $produkModel->delete($id);
		session()->setFlashdata('produk_delete', 'Berhasil dihapus');
        return redirect()->to(base_url('admin/produk'));
    }
	
	
	
	//--------------------------------------------------------------------

}
