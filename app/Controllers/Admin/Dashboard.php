<?php namespace App\Controllers\Admin;
use CodeIgniter\Controller;
use App\Models\MDashboard;
use CodeIgniter\I18n\Time;

class Dashboard extends Controller
{
	public function __construct()
    {
		helper('form');
		helper('date');
		$this->validation = \Config\Services::validation();
		$this->session = session();
		
	}
	public function index()
	{
		 //cek apakah ada session bernama isLogin
		 if(!$this->session->has('isLogin')){
            return redirect()->to(base_url('auth/login'));
        }
        
        //cek role dari session
        // if($this->session->get('role') != 1){
        //     return redirect()->to(base_url('user'));
        // }
		

		return render('admin/dashboard/dashboard');    
	}

	
	
	
	
	//--------------------------------------------------------------------

}
