<?php namespace Config;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var array
	 */
	public $ruleSets = [
		\CodeIgniter\Validation\Rules::class,
		\CodeIgniter\Validation\FormatRules::class,
		\CodeIgniter\Validation\FileRules::class,
		\CodeIgniter\Validation\CreditCardRules::class,
	];

	public $login = [
		'username' => [
		 'rules' => 'required|min_length[5]',
		],
		'password' => [
		 'rules' => 'required',
		],
	   ];
	  public $login_errors = [
		'username' => [
		 'required' =>'{field} Harus Diisi',
		 'min_length' => '{field} Minimal 5 Karakter',
		],
		'password' => [
		 'required' => '{field} Harus Diisi',
		],
	   ];

	     // Rules
    //--------------------------------------------------------------------
        
    public $register = [
		'email' => 'required|valid_email|is_unique[customer.email]',
        'username' => 'alpha_numeric|is_unique[customer.username]',
        'password' => 'min_length[8]|alpha_numeric_punct',
        'confirm' => 'matches[password]'
        ];

        
   public $register_errors = [
		
	   'email' => [
			'required' => 'Email wajib diisi',
			'valid_email' => 'Email tidak valid',
			'is_unique' => 'Email sudah dipakai'
		],
       'username' => [
           'alpha_numeric' => 'Username hanya boleh mengandung huruf dan angka',
           'is_unique' => 'Username sudah dipakai'
           ],
        'password' => [
            'min_length' => 'Password harus terdiri dari 8 kata',
            'alpha_numeric_punct' => 'Password hanya boleh mengandung angka, huruf, dan karakter yang valid'
            ],
       'confirm' => [
           'matches' => 'Konfirmasi password tidak cocok'
           ]
       ];

	    public $update_profil = [
		'images' => 'mime_in[images,image/png,image/jpg,image/jpeg]|max_size[images,400]',
		];

		public $update_profil_errors = [
			'images' => [ 
				'max_size' => 'Ukuran gambar terlalu besar'
			]
			];

	   

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------
}
