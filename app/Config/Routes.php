<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Login');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.


// $routes->get('/', 'Auth/Login::index');
$routes->get('/', 'Frontend/Home::index');
$routes->get('/auth/register', 'Auth/Auth::register');
$routes->get('/auth/login', 'Auth/Auth::login');
$routes->add('/auth/valid_login', 'Auth/Auth::valid_login');
$routes->add('/auth/logout', 'Auth/Auth::logout');
$routes->add('/auth/valid_register', 'Auth/Auth::valid_register');
$routes->add('/profil', 'Frontend/Profil::index');
$routes->add('/update_profil', 'Frontend/Profil::store_update');


$routes->get('/admin', 'Auth/Admin::login');
$routes->add('/admin/valid_register', 'Auth/Admin::valid_register');
$routes->add('/admin/valid_login', 'Auth/Admin::valid_login');
$routes->get('/admin/dashboard', 'Admin/Dashboard::index');
$routes->add('/admin/logout', 'Auth/Admin::logout');

$routes->get('/admin/produk', 'Admin/Produk::index');
$routes->get('/admin/produk/store_produk', 'Admin/Produk::store_produk');
$routes->get('/admin/produk/getKaregoriProduk', 'Admin/Produk::getKaregoriProduk');
$routes->get('/admin/produk/store_edit_produk', 'Admin/Produk::store_edit_produk');



/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
